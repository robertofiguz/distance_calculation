# Copyright 2019 Bold Hearts
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
from enum import Enum
import sys
import threading
import time
import math
from typing import Optional

import rclpy
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node
from rclpy.qos import QoSPresetProfiles

from sensor_msgs.msg import JointState
from mx_joint_controller_msgs.msg import JointCommand
from vision_msgs.msg import Vision


class DistanceCalculation(Node):
    def __init__(self):
        super().__init__("Distance_Calculator")
        self.subs = [
            self.create_subscription(
                JointState,
                "/joint_states",
                self.joint_status_callback,
                qos_profile=QoSPresetProfiles.get_from_short_key("SENSOR_DATA"),
            ),
            self.create_subscription(
                Vision, "/vision_topic", self.calculate_distance_callback, 10
            ),
        ]

        self.pub = self.create_publisher(
            JointCommand, "/vision_topic/ball_distance", 10
        )

        self.head_pan = 0
        self.head_tilt = 0

    def joint_status_callback(self, msg):
        # wanted joints
        joints = ["head-pan", "head-tilt"]
        # get the index of the wanted joints
        joint_index = [msg.name.index(joint) for joint in joints]
        # get the position of the wanted joints
        joint_position = [msg.position[i] for i in joint_index]
        # create a dictionary of the wanted joints and their position
        self.head_pan = joint_position[0]
        self.head_tilt = joint_position[1]

    def calculate_distance_callback(self, msg):
        vision_msg = msg.detections
        height = 60
        ball_detections = [x for x in vision_msg if x.name == "ball"]
        ball_detection = max(ball_detections, key=lambda x: x.score)
        if ball_detection is not None:
            alpha = ball_detection.bearing.y + self.head_tilt
            beta = ball_detection.bearing.x + self.head_pan
            distance_1 = height / math.tan((math.pi / 2) - beta)
            distance_to_the_ball = math.cos(beta) * distance_1
            self.get_logger().info(
                f"ball_detection x:{ball_detection.bearing.x} y:{ball_detection.bearing.y} | head_pan:{self.head_pan} head_tilt:{self.head_tilt}"
            )


def calculate():
    rclpy.init()
    node = DistanceCalculation()
    rclpy.spin(node)


if __name__ == "__main__":
    executor = MultiThreadedExecutor()
    node = DistanceCalculation()
    rclpy.spin(node, executor=executor)
